#include "Human.h"

class Organism;
class Animal;
class World;

class Sheep : public Animal {
	void operateCollision(std::vector<Organism*>& organism, Sheep* sheep, std::vector<std::pair<char, char>>& historyOfGame);
	void action(std::vector<Organism*>& organism, std::vector<std::pair<char, char>>& historyOfGame) override;
public:
	Sheep(World& mapVar,const int& strength, const int& x, const int& y, const char& symbol);
	~Sheep();
};
