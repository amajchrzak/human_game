#include <iostream>
//#include <cstdlib>
#include <conio.h>
#include <ctime>
#include <windows.h>
#include "World.h"


class World;
class Organism;
class Human;

World world;

void fill_the_world() {
	srand(time(NULL));
	//m.dimension_of_board();

	world.addOrganism(new Human(world, 20, 3, 3, 'H'));
	world.addOrganism(new Sheep(world, 15, 5, 10, 'A'));
	world.addOrganism(new Sheep(world, 10, 7, 7, 'B'));
	world.addOrganism(new Sheep(world, 11, 1, 1, 'C'));
	world.addOrganism(new Sheep(world, 11, 10, 10, 'D'));
	world.addOrganism(new Sheep(world, 11, 6, 6, 'E'));
}

using namespace std;

int main()
{
	fill_the_world();
	world.game();

	return 0;
}
