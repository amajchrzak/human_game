#include "Animal.h"


class Organism;
class Animal;
class World;

class Human : public Animal {
	int upper, lower, right, left, inputButton;
	void action(std::vector<Organism*>& organism, std::vector<std::pair<char, char>>& historyOfGame) override;
public:
	Human(World& mapVar,const int& strength, const int& x, const int& y, const char& symbol);
	~Human();
	
	const void controlButtons();
};
