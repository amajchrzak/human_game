﻿#include "World.h"
#include "includes.h"


static void gotoxy(int x, int y) {
	COORD position;
	position.X = x + 1;
	position.Y = y + 1;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), position);
};

void World::game() {
    for (;;) {
		system("cls");
        drawTheWorld();
        Sleep(100);
		round();
    }
}

void World::drawTheWorld() {
    for (int i = 0; i <= width; ++i) {
        std::cout << star;
    }
    std::cout << star;
    for (int i = 0; i < height; ++i) {
        //rysuje lewy bok
        std::cout << std::endl << star;

        for (int j = 0; j < width; ++j) {
            std::cout << fields[j][i];
			newFields[j][i] = fields[j][i];
        }
        //rysuje prawy bok
        std::cout << star;
    }

    std::cout << std::endl;
    std::cout << star;
    //rysuje doln¹ ramkê
    for (int i = 0; i < width; ++i) {
        std::cout << star;
    }
    std::cout << star;
}

void World::drawNewWorld(std::vector<std::pair<char, char>>& historyOfGame)
{
	for (size_t yR = 0; yR < height; yR++) {
		for (size_t xR = 0; xR < width; xR++) {
			if (fields[yR][xR] != newFields[yR][xR]) {
				gotoxy(xR, yR);
				std::cout << fields[yR][xR];
				newFields[yR][xR] = fields[yR][xR];
			}
		}
	}
	for (auto it = historyOfGame.cbegin(); it != historyOfGame.cend(); ++it)
	{
		std::cout << std::endl << it->first << " zabił: " << it->second << "\n";
	}

}

const int World::getHeight()
{
	return this->height;
}

const int World::getWidth()
{
	return this->width;
}

const int World::getField(const int& widthField, const int& heightField)
{
	return this->fields[widthField][heightField];
}

const void World::setField(const int& widthField, const int& heightField, const char& symbol)
{
	this->fields[widthField][heightField] = symbol;
}

const void World::addOrganism(Organism* new_organism)
{
    allOrganisms.push_back(new_organism);
    fields[new_organism->getCoordinateX()][new_organism->getCoordinateY()] = new_organism->getSymbol();
}

const void World::deleteOrganism(Organism* organismToDelete, std::vector<Organism*>& allOrganisms)
{
	int i = 0;
	for(const auto& iterator : allOrganisms)
	{
		if (organismToDelete->getSymbol() == iterator->getSymbol())
		{
			delete allOrganisms[i];
			allOrganisms.erase(allOrganisms.begin() + i);
			break;
		}
		i++;
	}
}



const void World::round()
{
	howManyMoves++;
	for (const auto& iterator : allOrganisms) {
		iterator->action(allOrganisms, historyOfGame);
	}

	for (const auto& iterator : allOrganisms) {
		fields[iterator->getCoordinateX()][iterator->getCoordinateY()] = ' ';
	}
	for (const auto& iterator : allOrganisms) {
		if (fields[iterator->getCoordinateX()][iterator->getCoordinateY()] != iterator->getSymbol())
			fields[iterator->getCoordinateX()][iterator->getCoordinateY()] = iterator->getSymbol();
	}

}



//funkcje do użycia w przyszłosci

//void Map::check_errors(int& x) {
//    while ((x < 5) || (x > 30)) {
//        std::cout << "wrong scope ! try again";
//        std::cin >> x;
//    }
//}

//void Map::dimension_of_board() {
//    std::cout << "Enter the width the scope is [5, 30]";
//    std::cin >> width;
//    check_errors(width);
//    std::cout << "Enter the height the scope is [5,30]";
//    std::cin >> height;
//    check_errors(height);
//}

//void Map::key_to_play() {
//    std::cout << "Enter key which you want to play: " << std::endl;
//    std::cout << "Upper" << std::endl;
//    upper = _getch();
//    if (upper == 224) upper += _getch();
//    if (upper == 0) upper -= _getch();
//
//    std::cout << "Lower" << std::endl;
//    lower = _getch();
//    if (lower == 224) lower += _getch();
//    if (lower == 0) lower -= _getch();
//
//    std::cout << "Right" << std::endl;
//    right = _getch();
//    if (right == 224) right += _getch();
//    if (right == 0) right -= _getch();
//
//    std::cout << "Left" << std::endl;
//    left = _getch();
//    if (left == 224) left += _getch();
//    if (left == 0) left -= _getch();
//}

