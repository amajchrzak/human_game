#include <iostream>
#include <conio.h>
#include "World.h"


Human::Human(World& mapVar,const int& strength, const int& x, const int& y, const char& symbol) : Animal(mapVar,strength, x, y, symbol)
{
}

Human::~Human()
{
}

const void Human::controlButtons(){
	upper = 224;
	left = 224;
	right = 224;
	lower = 224;

	if (upper == 224) {
		upper += 72;
	}

	if (lower == 224) {
		lower += 80;
	}

	if (right == 224) {
		right += 77;
	}

	if (left == 224) {
		left += 75;
	}
}

void Human::action(std::vector<Organism*>& organism, std::vector<std::pair<char, char>>& historyOfGame)
{
	this->world.drawNewWorld(historyOfGame);

	controlButtons();

	while (true) {
		if (_kbhit()) {
			inputButton = _getch();
			if (inputButton == 224) {
				inputButton += _getch();
			}

			if (inputButton == 0) {
				inputButton -= _getch();
			}

			if (inputButton == lower) {
				this->world.setField(this->getCoordinateX(), this->getCoordinateY(), ' ');
				
				this->setCoordinateY(getCoordinateY() + 1);
			}

			if (inputButton == upper) {
				this->world.setField(this->getCoordinateX(), this->getCoordinateY(), ' ');
				
				this->setCoordinateY(getCoordinateY() - 1);
			}

			if (inputButton == left) {
				this->world.setField(this->getCoordinateX(), this->getCoordinateY(), ' ');
				
				this->setCoordinateX(getCoordinateX() - 1);
			}

			if (inputButton == right) {
				this->world.setField(this->getCoordinateX(), this->getCoordinateY(), ' ');
				
				this->setCoordinateX(getCoordinateX() + 1);
			}

			if (this->getCoordinateX() >= this->world.getWidth()) {
				this->setCoordinateX(getCoordinateX() - 1);
			}
			if (this->getCoordinateX() < 0) {
				this->setCoordinateX(getCoordinateX() + 1);
			}
			if (this->getCoordinateY() >= this->world.getHeight()) {
				this->setCoordinateY(getCoordinateY() - 1);
			}
			if (this->getCoordinateY() < 0) {
				this->setCoordinateY(getCoordinateY() + 1); 
			}
			return;
		}
	}
}
