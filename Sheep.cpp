#include "World.h"
#include <time.h>
#include <Windows.h>
#include <iostream>


Sheep::Sheep(World& mapVar,const int& strength, const int& x, const int& y, const char& symbol) : Animal(mapVar,strength, x, y, symbol)
{
}

Sheep::~Sheep()
{
}

void Sheep::operateCollision(std::vector<Organism*>& organism, Sheep* sheep, std::vector<std::pair<char, char>>& historyOfGame)
{
	World object;
	for (auto iterator : organism)
	{

		if (this->getCoordinateX() == iterator->getCoordinateX() && this->getCoordinateY() == iterator->getCoordinateY() && this->getSymbol() != iterator->getSymbol()) {
			char tempSymbolOfOrganism = iterator->getStrength();
			if (this->getStrength() > iterator->getStrength()) {
				historyOfGame.push_back(std::make_pair(this->getSymbol(), iterator->getSymbol()));
				object.deleteOrganism(iterator, organism);
				break;
			} else if (this->getStrength() == iterator->getStrength()) {
				continue;
			} else {
				historyOfGame.push_back(std::make_pair(iterator->getSymbol(), this->getSymbol()));
				object.deleteOrganism(this, organism);
				break;
			}
		}
	}
}

void Sheep::action(std::vector<Organism*>& organism, std::vector<std::pair<char, char>>& historyOfGame)
{
	//randomly decide side to move by sheeps
	int randomNumber = rand() % 4 + 1;

	switch (randomNumber) {
		case 1:
			this->world.setField(this->getCoordinateX(), this->getCoordinateY(), ' ');
		
			this->setCoordinateX(getCoordinateX() - 1);

			if (this->getCoordinateX() < 0) {
				this->setCoordinateX(getCoordinateX() + 1);
			}

			operateCollision(organism, this, historyOfGame);

			break;
		case 2:
			this->world.setField(this->getCoordinateX(), this->getCoordinateY(), ' ');
		
			this->setCoordinateX(getCoordinateX() + 1);
		
			if (this->getCoordinateX() >= this->world.getWidth()) {
				this->setCoordinateX(getCoordinateX() - 1);
			}
		
			operateCollision(organism, this, historyOfGame);
		
			break;
		case 3:
			this->world.setField(this->getCoordinateX(), this->getCoordinateY(), ' ');
		
			this->setCoordinateY(getCoordinateY() + 1);
		
			if (this->getCoordinateY() >= this->world.getHeight()) {
				this->setCoordinateY(getCoordinateY() - 1);
			}

			operateCollision(organism, this, historyOfGame);
		
			break;
		case 4:
			this->world.setField(this->getCoordinateX(), this->getCoordinateY(), ' ');

			this->setCoordinateY(getCoordinateY() - 1);

			if (this->getCoordinateY() < 0) {
				this->setCoordinateY(getCoordinateY() + 1);
			}

			operateCollision(organism, this, historyOfGame);

			break;
	}
}