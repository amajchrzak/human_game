#include <vector>
#include "Sheep.h"

class Organism;

class World {
	std::vector<Organism*> allOrganisms;
	std::vector<std::pair<char, char>> historyOfGame;

	void checkErrors(int& x);
protected:
	//int coordinateX;
	//int coordinate_y;
	
	int upper, lower, right, left;
	//int dimension = 1;
	int historyCoordinateX[10000];
	int historyCoordinateY[10000];
	int howManyMoves = 0;
	const char star = 42;
	char fields[30][30];
	char newFields[30][30];
public:
	const int width = 15;
	const int height = 15;
	void drawTheWorld();
	void drawNewWorld(std::vector<std::pair<char, char>>& historyOfGame);
	//void dimension_of_board();
	void game();

	const int getHeight();
	const int getWidth();


	const int getField(const int& widthField, const int& heightField);
	const void setField(const int& widthField, const int& heightField, const char& symbol);

	const void addOrganism(Organism* new_organism);
	const void deleteOrganism(Organism* organismToDelete, std::vector<Organism*>& allOrganisms);
	const void round();
};

