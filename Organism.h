class World;

class Organism {
	int widthCoord, heightCoord;
	char symbol;
	const int strength;
protected:
	World& world;
public:
	/*Map & map;*/
	Organism(World & mapVar,const int& strength ,const int& x,const int& y,  const char& symbol);
	~Organism();
	//nadaj wsp�rz�dne
	const int getStrength();
	const int getCoordinateX();
	const int getCoordinateY();
	const char getSymbol();

	//ustaw na mapie
	void setCoordinateX(int new_x);
	void setCoordinateY(int new_y);

	virtual void action(std::vector<Organism*>& organism, std::vector<std::pair<char, char>>& historyOfGame) = 0;

	/*const bool operator <(const Organism& s2) const;
	const bool operator ==(const Organism& s2) const;*/
};