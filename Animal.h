#include "Organism.h"

class Organism;
class World;
class Animal;

class Animal : public Organism {
public:
	Animal(World& mapVar, const int& strength, const int& x, const int& y, const char& symbol);
	~Animal();
	virtual void action(std::vector<Organism*>& organism, std::vector<std::pair<char, char>>& historyOfGame) = 0;
};