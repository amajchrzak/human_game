#include <iostream>
#include "World.h"


Organism::Organism(World& mapVar,const int& strength, const int& x, const int& y,const char& symbol)
	: world(mapVar),strength(strength), widthCoord(x), heightCoord(y), symbol(symbol)
{
}

Organism::~Organism()
{
}

const int Organism::getStrength()
{
	return this->strength;
}

const int Organism::getCoordinateX()
{
	return this->widthCoord;
}

const int Organism::getCoordinateY()
{
	return this->heightCoord;
}

const char Organism::getSymbol()
{
	return this->symbol;
}

void Organism::setCoordinateX(int newWidth)
{
	this->widthCoord = newWidth;
}

void Organism::setCoordinateY(int newHeight)
{
	this->heightCoord = newHeight;
}

//const bool Organism::operator<(const Organism& s2) const {
//    if (this->initiative == s2.initiative)
//        return this->age < s2.age;
//    else
//        return this->initiative < s2.initiative;
//}
//
//const bool Organism::operator==(const Organism& s2) const {
//    if (this->initiative == s2.initiative)
//        return this->age == s2.age;
//    else
//        return this->initiative == s2.initiative;
//}